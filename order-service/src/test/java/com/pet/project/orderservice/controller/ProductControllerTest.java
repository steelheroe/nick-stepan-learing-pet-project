package com.pet.project.orderservice.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.fge.jsonpatch.JsonPatch;
import com.pet.project.orderservice.domain.dto.product.GetProductResponse;
import com.pet.project.orderservice.domain.dto.product.SaveProductRequest;
import com.pet.project.orderservice.service.ProductService;
import com.pet.project.orderservice.util.mapping.ProductMapperImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;

import static com.pet.project.orderservice.TestUtil.asJsonString;
import static com.pet.project.orderservice.TestUtil.convertToModel;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@Import(ProductMapperImpl.class)
@WebMvcTest(ProductController.class)
class ProductControllerTest {

  private static final String BASE_PRODUCT_RESOURCE_URL = "/products";

  @Autowired
  private MockMvc mvc;

  @MockBean
  ProductService productService;

  @Test
  void whenCallGetProductById_thenGetSuccessfulResponse() throws Exception {
    //given
    when(productService.findById(anyLong())).thenAnswer(invocation ->
        new GetProductResponse().setCode(invocation.getArgument(0)).setName("Name????")
    );

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_PRODUCT_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetProductResponse>() {
    })).isNotNull();
  }

  @Test
  public void whenCallGetProducts_thenGetSuccessfulResponse() throws Exception {
    //given
    when(productService.findAll()).thenReturn(Lists.list(
        new GetProductResponse().setCode(123L).setName("Name with 123"),
        new GetProductResponse().setCode(1233L).setName("Name with 12523"),
        new GetProductResponse().setCode(12L).setName("Name 12523")
    ));

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_PRODUCT_RESOURCE_URL)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<List<GetProductResponse>>() {
    }).size())
        .isEqualTo(3);
  }

  @Test
  public void whenCallSaveProduct_thenGetSuccessfulResponse() throws Exception {
    //given
    when(productService.save(any(SaveProductRequest.class))).thenAnswer(invocation -> {
          SaveProductRequest result = invocation.getArgument(0);
          return new GetProductResponse()
              .setCode(123L)
              .setName(result.getName())
              .setDescription(result.getDescription());
        }
    );

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .post(BASE_PRODUCT_RESOURCE_URL)
        .content(asJsonString(new SaveProductRequest().setName("anme").setDescription("asdasd")))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(201);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetProductResponse>() {
    })).isNotNull();
  }

  @Test
  void whenCallDeleteProduct_thenGetSuccessfulResponse() throws Exception {
    //given

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .delete(BASE_PRODUCT_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
  }

  @Test
  public void whenCallUpdateProduct_thenGetSuccessfulResponse() throws Exception {
    //given
    when(productService.findById(anyLong())).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
          return new GetProductResponse()
              .setCode(id)
              .setName("Name")
              .setDescription("Description")
              .setPrice(BigDecimal.TEN)
              .setQuantityInStock(100);
        }
    );
    when(productService.update(anyLong(), any(SaveProductRequest.class))).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
          SaveProductRequest savedProduct = invocation.getArgument(1);
          return new GetProductResponse()
              .setCode(id)
              .setName(savedProduct.getName())
              .setDescription(savedProduct.getDescription())
              .setPrice(savedProduct.getPrice())
              .setQuantityInStock(savedProduct.getQuantityInStock());
        }
    );

    //when
    String newName = "Ahaha";

    MockHttpServletResponse existingEntityResponse = mvc.perform(MockMvcRequestBuilders
        .get(BASE_PRODUCT_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    GetProductResponse entity = convertToModel(existingEntityResponse.getContentAsString(), new TypeReference<GetProductResponse>() {
    });
    entity.setName(newName);

    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .put(BASE_PRODUCT_RESOURCE_URL + "/" + entity.getCode())
        .content(asJsonString(entity))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetProductResponse>() {
    }).getName()).isEqualTo(newName);
  }

  @Test
  void whenCallPatchProduct_thenGetSuccessfulResponse() throws Exception {
    //given
    String newName = "Ahaha";

    when(productService.findById(anyLong())).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
          return new GetProductResponse()
              .setCode(id)
              .setName("Name")
              .setDescription("Description")
              .setPrice(BigDecimal.TEN)
              .setQuantityInStock(100);
        }
    );
    when(productService.patch(anyLong(), any(JsonPatch.class))).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
          GetProductResponse product = productService.findById(id);

          return new GetProductResponse()
              .setCode(product.getCode())
              .setName(newName)
              .setDescription(product.getDescription())
              .setPrice(product.getPrice())
              .setQuantityInStock(product.getQuantityInStock());
        }
    );

    //when
    MockHttpServletResponse existingEntityResponse = mvc.perform(MockMvcRequestBuilders
        .get(BASE_PRODUCT_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    GetProductResponse entity = convertToModel(existingEntityResponse.getContentAsString(), new TypeReference<GetProductResponse>() {});

    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .patch(BASE_PRODUCT_RESOURCE_URL + "/123")
        .content("[\n" +
            "    {\n" +
            "        \"op\": \"replace\",\n" +
            "        \"path\": \"/name\",\n" +
            "        \"value\": \"" + newName + "\"" +
            "    }\n" +
            "]")
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetProductResponse>() {
    }).getName()).isEqualTo(newName);
  }
}