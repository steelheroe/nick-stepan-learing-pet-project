package com.pet.project.orderservice.service;

import com.pet.project.orderservice.domain.dto.order.GetOrderResponse;
import com.pet.project.orderservice.domain.dto.order.SaveOrderRequest;
import com.pet.project.orderservice.domain.entity.Order;
import com.pet.project.orderservice.domain.entity.OrderStatus;
import com.pet.project.orderservice.domain.entity.projection.OrderWithDetailsProductIdsProjection;
import com.pet.project.orderservice.persistence.repository.OrderRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.OrderMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

  @Mock
  OrderRepository orderRepository;

  @Spy
  OrderMapper orderMapper = Mappers.getMapper(OrderMapper.class);

  @InjectMocks
  private OrderService orderService;

  @Test
  void whenCallFindById_thenReturnFoundObject() {
    //given
    Order order = new Order()
        .setOrderNumber(1L)
        .setOrderDate(Timestamp.valueOf(LocalDateTime.now()))
        .setShippingDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)))
        .setStatus(OrderStatus.CREATED)
        .setComment("comment")
        .setCustomerName("name");
    when(orderRepository.findById(order.getOrderNumber())).thenReturn(Optional.of(order));

    //when
    GetOrderResponse foundOrder = orderService.findById(order.getOrderNumber());

    //then
    assertThat(foundOrder).isNotNull();
  }

  @Test
  void whenCallFindByIdForAbsentEntity_thenThrowException() {
    //given
    when(orderRepository.findById(anyLong())).thenReturn(Optional.empty());

    //when
    EntityDoesNotExistException exception = assertThrows(EntityDoesNotExistException.class,
        () -> orderService.findById(1L));

    //then
    assertThat(exception.getMessage()).contains("does not exist");
  }

  @Test
  void whenCallFindAll_thenReturnFoundObjects() {
    //given
    LocalDateTime now = LocalDateTime.now();

    OrderWithDetailsProductIdsProjection order1 = mock(OrderWithDetailsProductIdsProjection.class);
    OrderWithDetailsProductIdsProjection order2 = mock(OrderWithDetailsProductIdsProjection.class);

    when(order1.getOrderNumber()).thenReturn(1L);
    when(order1.getOrderDate()).thenReturn(Timestamp.valueOf(now));
    when(order1.getShippingDate()).thenReturn(Timestamp.valueOf(now.plusMonths(1)));
    when(order1.getStatus()).thenReturn(OrderStatus.CREATED);
    when(order1.getComment()).thenReturn("comment");
    when(order1.getCustomerName()).thenReturn("name");

    when(order2.getOrderNumber()).thenReturn(2L);
    when(order2.getOrderDate()).thenReturn(Timestamp.valueOf(now));
    when(order2.getShippingDate()).thenReturn(Timestamp.valueOf(now.plusMonths(1)));
    when(order2.getStatus()).thenReturn(OrderStatus.CREATED);
    when(order2.getComment()).thenReturn("comment 2");
    when(order2.getCustomerName()).thenReturn("name 2");

    when(orderRepository.findAllProjectedBy()).thenReturn(Lists.list(order1, order2));

    //when
    ArrayList<GetOrderResponse> actualList = Lists.newArrayList(orderService.findAll());

    //then
    ArrayList<GetOrderResponse> expectedList = Lists.newArrayList(
        new GetOrderResponse()
            .setOrderNumber(1L)
            .setOrderDate(Timestamp.valueOf(now))
            .setShippingDate(Timestamp.valueOf(now.plusMonths(1)))
            .setStatus("CREATED")
            .setComment("comment")
            .setCustomerName("name")
            .setOrderDetailProductIDs(new HashSet<>()),
        new GetOrderResponse()
            .setOrderNumber(2L)
            .setOrderDate(Timestamp.valueOf(now))
            .setShippingDate(Timestamp.valueOf(now.plusMonths(1)))
            .setStatus("CREATED")
            .setComment("comment 2")
            .setCustomerName("name 2")
            .setOrderDetailProductIDs(new HashSet<>())
    );

    assertThat(actualList.containsAll(expectedList) && expectedList.containsAll(actualList)).isTrue();
  }

  @Test
  void whenCallSave_thenCheckItsSaved() {
    //given
    when(orderRepository.save(any(Order.class))).thenAnswer(invocation -> ((Order) invocation.getArgument(0)).setOrderNumber(123L));

    //when
    GetOrderResponse savedOrder = orderService.save(new SaveOrderRequest()
        .setOrderDate(Timestamp.valueOf(LocalDateTime.now()))
        .setShippingDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)))
        .setStatus("CREATED")
        .setComment("comment")
        .setCustomerName("name"));

    //then
    assertThat(savedOrder.getOrderNumber()).isNotNull();
  }

  @Test
  void whenCallDelete_thenNothing() {
    //given

    //when
    orderService.delete(1L);

    //then
  }

  @Test
  void whenCallUpdate_thenReturnUpdatedEntity() {
    //given
    when(orderRepository.save(any(Order.class))).thenAnswer(invocation -> invocation.getArgument(0));

    //when
    GetOrderResponse updatedOrder = orderService.update(1, new SaveOrderRequest()
        .setOrderDate(Timestamp.valueOf(LocalDateTime.now()))
        .setShippingDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)))
        .setStatus("CREATED")
        .setComment("comment")
        .setCustomerName("name"));

    //then
    assertThat(updatedOrder).isNotNull();
  }
}