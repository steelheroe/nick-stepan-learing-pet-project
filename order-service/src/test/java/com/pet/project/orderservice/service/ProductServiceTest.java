package com.pet.project.orderservice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.pet.project.orderservice.domain.dto.product.GetProductResponse;
import com.pet.project.orderservice.domain.dto.product.SaveProductRequest;
import com.pet.project.orderservice.domain.entity.Product;
import com.pet.project.orderservice.persistence.repository.ProductRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.ProductMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTest {

  @Mock
  ProductRepository productRepository;

  @Spy
  ProductMapper productMapper = Mappers.getMapper(ProductMapper.class);

  @Spy
  ObjectMapper objectMapper;

  @InjectMocks
  private ProductService productService;

  @Test
  void whenCallFindById_thenReturnFoundObject() {
    //given
    Product product1 = new Product().setCode(1L).setName("Name 1").setDescription("Desc 1");
    when(productRepository.findById(product1.getCode())).thenReturn(Optional.of(product1));

    //when
    GetProductResponse foundProduct = productService.findById(product1.getCode());

    //then
    assertThat(foundProduct).isNotNull();
  }

  @Test
  void whenCallFindByIdForAbsentEntity_thenThrowException() {
    //given
    when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

    //when
    EntityDoesNotExistException exception = assertThrows(EntityDoesNotExistException.class,
        () -> productService.findById(1L));

    //then
    assertThat(exception.getMessage()).contains("does not exist");
  }

  @Test
  void whenCallFindAll_thenReturnFoundObjects() {
    //given
    Product product1 = new Product().setCode(1L).setName("Name 1").setDescription("Desc 1");
    Product product2 = new Product().setCode(2L).setName("Name 2").setDescription("Desc 2");
    when(productRepository.findAll()).thenReturn(Lists.list(product1, product2));

    //when
    ArrayList<GetProductResponse> actualList = Lists.newArrayList(productService.findAll());

    //then
    ArrayList<GetProductResponse> expectedList = Lists.newArrayList(
        new GetProductResponse().setCode(1L).setName("Name 1").setDescription("Desc 1"),
        new GetProductResponse().setCode(2L).setName("Name 2").setDescription("Desc 2")
    );

    assertThat(actualList.containsAll(expectedList) && expectedList.containsAll(actualList)).isTrue();
  }

  @Test
  void whenCallSave_thenCheckItsSaved() {
    //given
    when(productRepository.save(any(Product.class))).thenAnswer(invocation -> ((Product) invocation.getArgument(0)).setCode(123L));

    //when
    GetProductResponse savedProduct = productService.save(new SaveProductRequest());

    //then
    assertThat(savedProduct.getCode()).isNotNull();
  }

  @Test
  void whenCallDelete_thenNothing() {
    //given

    //when
    productService.delete(1L);

    //then
  }

  @Test
  void whenCallUpdate_thenReturnUpdatedEntity() {
    //given
    when(productRepository.save(any(Product.class))).thenAnswer(invocation -> invocation.getArgument(0));

    //when
    GetProductResponse updatedProduct = productService.update(1, new SaveProductRequest());

    //then
    assertThat(updatedProduct).isNotNull();
  }

  @Test
  void whenCallPatch_thenReturnUpdatedEntity() throws IOException, JsonPatchException {
    //given
    Product product = new Product().setCode(1L).setName("Name 1").setDescription("Desc 1");
    when(productRepository.findById(product.getCode())).thenReturn(Optional.of(product));
    when(productRepository.save(any(Product.class))).thenAnswer(invocation -> invocation.getArgument(0));

    //when
    String newName = "Totally new name";

    GetProductResponse response = productService.patch(1L, JsonPatch.fromJson(objectMapper.readTree("[\n" +
        "    {\n" +
        "        \"op\": \"replace\",\n" +
        "        \"path\": \"/name\",\n" +
        "        \"value\": \"" + newName + "\"" +
        "    }\n" +
        "]")));

    //then
    assertThat(response.getName()).isEqualTo(newName);
  }

  @Test
  void whenCallPatchForAbsentEntity_thenThrowException() throws IOException, JsonPatchException {
    //given
    when(productRepository.findById(anyLong())).thenReturn(Optional.empty());

    //when
    EntityDoesNotExistException exception = assertThrows(EntityDoesNotExistException.class,
        () -> productService.patch(1L, JsonPatch.fromJson(objectMapper.readTree("[\n" +
            "    {\n" +
            "        \"op\": \"replace\",\n" +
            "        \"path\": \"/name\",\n" +
            "        \"value\": \"New Name\"" +
            "    }\n" +
            "]"))));

    //then
    assertThat(exception.getMessage()).contains("does not exist");
  }
}