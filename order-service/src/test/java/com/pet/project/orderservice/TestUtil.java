package com.pet.project.orderservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestUtil {

  private static final ObjectMapper objectMapper = new ObjectMapper();

  public static <T> String asJsonString(T t) throws JsonProcessingException {
    return objectMapper.writeValueAsString(t);
  }

  public static <T> T convertToModel(String value, TypeReference<T> typeReference) throws JsonProcessingException {
    return objectMapper.readValue(value, typeReference);
  }
}
