package com.pet.project.orderservice.service;

import com.pet.project.orderservice.domain.dto.orderdetail.CreateUpdateOrderDetailRequest;
import com.pet.project.orderservice.domain.dto.orderdetail.GetOrderDetailResponse;
import com.pet.project.orderservice.domain.entity.OrderDetail;
import com.pet.project.orderservice.domain.entity.OrderDetailPK;
import com.pet.project.orderservice.persistence.repository.OrderDetailRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.OrderDetailMapper;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class OrderDetailServiceTest {

  @Mock
  OrderDetailRepository orderDetailRepository;

  @Spy
  OrderDetailMapper orderMapper = Mappers.getMapper(OrderDetailMapper.class);

  @InjectMocks
  private OrderDetailService orderDetailService;

  @Test
  void whenCallFindById_thenReturnFoundObject() {
    //given
    OrderDetail orderDetail = new OrderDetail()
        .setId(new OrderDetailPK().setOrderNumber(1L).setProductNumber(1L))
        .setPriceForEach(BigDecimal.TEN)
        .setQuantityOrdered(2);
    when(orderDetailRepository.findById(orderDetail.getId())).thenReturn(Optional.of(orderDetail));

    //when
    GetOrderDetailResponse foundOrderDetail = orderDetailService.findById(1L, 1L);

    //then
    assertThat(foundOrderDetail).isNotNull();
  }

  @Test
  void whenCallFindByIdForAbsentEntity_thenThrowException() {
    //given
    when(orderDetailRepository.findById(any(OrderDetailPK.class))).thenReturn(Optional.empty());

    //when
    EntityDoesNotExistException exception = assertThrows(EntityDoesNotExistException.class,
        () -> orderDetailService.findById(1L, 1L));

    //then
    assertThat(exception.getMessage()).contains("does not exist");
  }

  @Test
  void whenCallFindByOrderId_thenReturnFoundObjects() {
    //given
    OrderDetail orderDetail1 = new OrderDetail()
        .setId(new OrderDetailPK().setOrderNumber(1L).setProductNumber(1L))
        .setPriceForEach(BigDecimal.TEN)
        .setQuantityOrdered(2);
    OrderDetail orderDetail2 = new OrderDetail()
        .setId(new OrderDetailPK().setOrderNumber(1L).setProductNumber(2L))
        .setPriceForEach(BigDecimal.ONE)
        .setQuantityOrdered(1);
    when(orderDetailRepository.findById_OrderNumber(1L)).thenReturn(Lists.newArrayList(orderDetail1, orderDetail2));

    //when
    List<GetOrderDetailResponse> foundOrderDetails = orderDetailService.findAllByOrderNumber(1L);

    //then
    assertThat(foundOrderDetails.size()).isEqualTo(2);
  }

  @Test
  void whenCallDelete_thenNothing() {
    //given

    //when
    orderDetailService.delete(1L, 1L);

    //then
  }

  @Test
  void whenCallDeleteByOrderId_thenNothing() {
    //given

    //when
    orderDetailService.deleteByOrderNumber(1L);

    //then
  }

  @Test
  void whenCallUpdate_thenReturnUpdatedEntity() {
    //given
    when(orderDetailRepository.save(any(OrderDetail.class))).thenAnswer(invocation -> invocation.getArgument(0));

    //when
    GetOrderDetailResponse updatedOrderDetail = orderDetailService.update(1L, 2L, new CreateUpdateOrderDetailRequest()
        .setPriceForEach(BigDecimal.TEN)
        .setQuantityOrdered(2));

    //then
    assertThat(updatedOrderDetail).isNotNull();
  }
}