package com.pet.project.orderservice.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.pet.project.orderservice.domain.dto.orderdetail.CreateUpdateOrderDetailRequest;
import com.pet.project.orderservice.domain.dto.orderdetail.GetOrderDetailResponse;
import com.pet.project.orderservice.service.OrderDetailService;
import com.pet.project.orderservice.util.mapping.OrderDetailMapperImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;

import static com.pet.project.orderservice.TestUtil.asJsonString;
import static com.pet.project.orderservice.TestUtil.convertToModel;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@Import({OrderDetailMapperImpl.class})
@WebMvcTest(OrderDetailController.class)
class OrderDetailControllerTest {

  private static final Long ORDER_NUMBER = 1L;
  private static final String BASE_ORDER_DETAIL_RESOURCE_URL = "/orders/" + ORDER_NUMBER + "/orderDetails";

  @Autowired
  private MockMvc mvc;

  @MockBean
  OrderDetailService orderDetailService;

  @Test
  void whenCallGetOrderDetailById_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderDetailService.findById(eq(ORDER_NUMBER), anyLong())).thenAnswer(invocation ->
        new GetOrderDetailResponse()
            .setOrderNumber(ORDER_NUMBER)
            .setProductNumber(invocation.getArgument(1))
            .setQuantityOrdered(2)
            .setPriceForEach(BigDecimal.TEN)
    );

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_ORDER_DETAIL_RESOURCE_URL + "/1")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetOrderDetailResponse>() {
    })).isNotNull();
  }

  @Test
  public void whenCallGetOrders_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderDetailService.findAllByOrderNumber(eq(ORDER_NUMBER))).thenReturn(Lists.list(
        new GetOrderDetailResponse()
            .setOrderNumber(ORDER_NUMBER)
            .setProductNumber(1)
            .setQuantityOrdered(2)
            .setPriceForEach(BigDecimal.TEN),
        new GetOrderDetailResponse()
            .setOrderNumber(ORDER_NUMBER)
            .setProductNumber(2)
            .setQuantityOrdered(2)
            .setPriceForEach(BigDecimal.ONE),
        new GetOrderDetailResponse()
            .setOrderNumber(ORDER_NUMBER)
            .setProductNumber(3)
            .setQuantityOrdered(2)
            .setPriceForEach(BigDecimal.TEN)
    ));

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_ORDER_DETAIL_RESOURCE_URL)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<List<GetOrderDetailResponse>>() {
    }).size())
        .isEqualTo(3);
  }

  @Test
  public void whenCallUpdateOrderDetail_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderDetailService.update(eq(ORDER_NUMBER), anyLong(), any(CreateUpdateOrderDetailRequest.class))).thenAnswer(invocation -> {
          Long id = invocation.getArgument(1);
          CreateUpdateOrderDetailRequest saveOrderDetail = invocation.getArgument(2);
          return new GetOrderDetailResponse()
              .setOrderNumber(ORDER_NUMBER)
              .setProductNumber(id)
              .setQuantityOrdered(saveOrderDetail.getQuantityOrdered())
              .setPriceForEach(saveOrderDetail.getPriceForEach());
        }
    );

    //when
    CreateUpdateOrderDetailRequest entity = new CreateUpdateOrderDetailRequest()
        .setQuantityOrdered(15L).setPriceForEach(BigDecimal.TEN);

    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .put(BASE_ORDER_DETAIL_RESOURCE_URL + "/" + 1)
        .content(asJsonString(entity))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetOrderDetailResponse>() {
    }).getPriceForEach()).isEqualTo(entity.getPriceForEach());
  }

  @Test
  void whenCallDeleteOrderDetailByOrderId_thenGetSuccessfulResponse() throws Exception {
    //given

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .delete(BASE_ORDER_DETAIL_RESOURCE_URL)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
  }

  @Test
  void whenCallDeleteOrderDetailByProductId_thenGetSuccessfulResponse() throws Exception {
    //given

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .delete(BASE_ORDER_DETAIL_RESOURCE_URL + "/1")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
  }
}