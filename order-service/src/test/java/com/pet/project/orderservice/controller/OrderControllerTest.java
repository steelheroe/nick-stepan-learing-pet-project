package com.pet.project.orderservice.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.pet.project.orderservice.domain.dto.order.GetOrderResponse;
import com.pet.project.orderservice.domain.dto.order.SaveOrderRequest;
import com.pet.project.orderservice.service.OrderService;
import com.pet.project.orderservice.util.mapping.OrderDetailMapperImpl;
import com.pet.project.orderservice.util.mapping.OrderMapperImpl;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

import static com.pet.project.orderservice.TestUtil.asJsonString;
import static com.pet.project.orderservice.TestUtil.convertToModel;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@Import({OrderMapperImpl.class, OrderDetailMapperImpl.class})
@WebMvcTest(OrderController.class)
class OrderControllerTest {

  private static final String BASE_ORDER_RESOURCE_URL = "/orders";

  @Autowired
  private MockMvc mvc;

  @MockBean
  OrderService orderService;

  @Test
  void whenCallGetOrderById_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderService.findById(anyLong())).thenAnswer(invocation ->
        new GetOrderResponse().setOrderNumber(invocation.getArgument(0)).setCustomerName("Name")
    );

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_ORDER_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetOrderResponse>() {
    })).isNotNull();
  }

  @Test
  public void whenCallGetOrders_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderService.findAll()).thenReturn(Lists.list(
        new GetOrderResponse().setOrderNumber(1L).setCustomerName("Name 1"),
        new GetOrderResponse().setOrderNumber(2L).setCustomerName("Name 2"),
        new GetOrderResponse().setOrderNumber(3L).setCustomerName("Name 3")
    ));

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .get(BASE_ORDER_RESOURCE_URL)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<List<GetOrderResponse>>() {
    }).size())
        .isEqualTo(3);
  }

  @Test
  public void whenCallSaveOrder_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderService.save(any(SaveOrderRequest.class))).thenAnswer(invocation -> {
      SaveOrderRequest result = invocation.getArgument(0);
          return new GetOrderResponse()
              .setOrderNumber(123L)
              .setCustomerName(result.getCustomerName())
              .setCustomerNumber(result.getCustomerNumber())
              .setOrderDate(result.getOrderDate())
              .setComment(result.getComment());
        }
    );

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .post(BASE_ORDER_RESOURCE_URL)
        .content(asJsonString(new SaveOrderRequest()
            .setOrderDate(Timestamp.valueOf(LocalDateTime.now()))
            .setShippingDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)))
            .setStatus("CREATED")
            .setComment("comment")
            .setCustomerName("name")))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(201);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetOrderResponse>() {
    })).isNotNull();
  }

  @Test
  void whenCallDeleteOrder_thenGetSuccessfulResponse() throws Exception {
    //given

    //when
    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .delete(BASE_ORDER_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
  }

  @Test
  public void whenCallUpdateOrder_thenGetSuccessfulResponse() throws Exception {
    //given
    when(orderService.findById(anyLong())).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
          return new GetOrderResponse()
              .setOrderNumber(id)
              .setOrderDate(Timestamp.valueOf(LocalDateTime.now()))
              .setShippingDate(Timestamp.valueOf(LocalDateTime.now().plusMonths(1)))
              .setStatus("CREATED")
              .setComment("comment")
              .setCustomerName("name");
        }
    );
    when(orderService.update(anyLong(), any(SaveOrderRequest.class))).thenAnswer(invocation -> {
          Long id = invocation.getArgument(0);
      SaveOrderRequest saveOrder = invocation.getArgument(1);
          return new GetOrderResponse()
              .setOrderDate(saveOrder.getOrderDate())
              .setShippingDate(saveOrder.getShippingDate())
              .setStatus(saveOrder.getStatus())
              .setComment(saveOrder.getComment())
              .setCustomerName(saveOrder.getCustomerName());
        }
    );

    //when
    String newName = "Ahaha";

    MockHttpServletResponse existingEntityResponse = mvc.perform(MockMvcRequestBuilders
        .get(BASE_ORDER_RESOURCE_URL + "/123")
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    GetOrderResponse entity = convertToModel(existingEntityResponse.getContentAsString(), new TypeReference<GetOrderResponse>() {
    });
    entity.setCustomerName(newName);

    MockHttpServletResponse response = mvc.perform(MockMvcRequestBuilders
        .put(BASE_ORDER_RESOURCE_URL + "/" + entity.getOrderNumber())
        .content(asJsonString(entity))
        .contentType(MediaType.APPLICATION_JSON)
        .accept(MediaType.APPLICATION_JSON)).andReturn().getResponse();

    //then
    assertThat(response.getStatus()).isEqualTo(200);
    assertThat(convertToModel(response.getContentAsString(), new TypeReference<GetOrderResponse>() {
    }).getCustomerName()).isEqualTo(newName);
  }
}