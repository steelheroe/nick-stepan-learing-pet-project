package com.pet.project.orderservice.persistence.repository;

import com.pet.project.orderservice.domain.entity.Product;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
class ProductRepositoryTest {

  @Autowired
  private TestEntityManager entityManager;

  @Autowired
  private ProductRepository productRepository;

  @Test
  void whenCallFindById_thenReturnFoundObject() {
    //given
    Product product = new Product()
        .setName("Name")
        .setDescription("Description")
        .setPrice(BigDecimal.TEN);

    entityManager.persist(product);
    entityManager.flush();

    //when
    Product foundProduct = productRepository.findById(product.getCode()).orElse(new Product());

    //then
    assertThat(foundProduct).isEqualTo(product);
  }

  @Test
  void whenCallFindAll_thenReturnFoundObjects() {
    //given
    Product product = new Product()
        .setName("Name")
        .setDescription("Description")
        .setPrice(BigDecimal.TEN);
    Product product1 = new Product()
        .setName("Name1")
        .setDescription("Description2")
        .setPrice(BigDecimal.ONE);

    productRepository.save(product);
    productRepository.save(product1);

    //when
    ArrayList<Product> actualList = Lists.newArrayList(productRepository.findAll());

    //then
    ArrayList<Product> expectedList = Lists.newArrayList(product1, product);

    assertThat(actualList.containsAll(expectedList) && expectedList.containsAll(actualList)).isTrue();
  }

  @Test
  void whenCallSave_thenReturnSavedObject() {
    //given
    Product product = new Product()
        .setName("Name")
        .setDescription("Description")
        .setPrice(BigDecimal.TEN);

    //when
    productRepository.save(product);

    //then
    Product foundProduct = productRepository.findById(product.getCode()).orElse(new Product());

    assertThat(foundProduct).isEqualTo(product);
  }

  @Test
  void whenCallDelete_thenCheckItIsDeleted() {
    //given
    Product product = new Product()
        .setName("Name")
        .setDescription("Description")
        .setPrice(BigDecimal.TEN);
    productRepository.save(product);

    //when
    productRepository.delete(product);

    //then
    assertThat(productRepository.findById(product.getCode())).isEqualTo(Optional.empty());
  }

  @Test
  void whenCallupdate_thenCheckItsUpdated() {
    //given
    Product product = new Product()
        .setName("Name")
        .setDescription("Description")
        .setPrice(BigDecimal.TEN);
    productRepository.save(product);

    //when
    String newName = "totally new name";
    product.setName(newName);

    //then
    assertThat(productRepository.findById(product.getCode()).get().getName()).isEqualTo(newName);
  }
}