package com.pet.project.orderservice.domain.dto.order;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
@Getter
@Accessors
public class SaveOrderRequest {

  @NotNull
  private Timestamp orderDate;

  @NotNull
  private Timestamp shippingDate;

  @NotNull
  private String status;

  private String comment;

  private String customerNumber;

  private String customerName;
}
