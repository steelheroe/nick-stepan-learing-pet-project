package com.pet.project.orderservice.util.mapping;

import com.pet.project.orderservice.domain.dto.product.GetProductResponse;
import com.pet.project.orderservice.domain.dto.product.SaveProductRequest;
import com.pet.project.orderservice.domain.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface ProductMapper {

  @Mapping(source = "name", target = "name")
  @Mapping(source = "price", target = "price")
  @Mapping(source = "description", target = "description")
  @Mapping(source = "quantityInStock", target = "quantityInStock")
  Product saveProductToProductMapping(SaveProductRequest source);

  List<Product> saveProductToProductMapping(List<SaveProductRequest> source);

  @Mapping(target = "code", source = "code")
  @Mapping(target = "name", source = "name")
  @Mapping(target = "price", source = "price")
  @Mapping(target = "description", source = "description")
  @Mapping(target = "quantityInStock", source = "quantityInStock")
  GetProductResponse productToGetProductResponse(Product source);

  List<GetProductResponse> productToGetProductResponse(List<Product> source);
}
