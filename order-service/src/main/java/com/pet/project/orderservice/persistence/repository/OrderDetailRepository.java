package com.pet.project.orderservice.persistence.repository;

import com.pet.project.orderservice.domain.entity.OrderDetail;
import com.pet.project.orderservice.domain.entity.OrderDetailPK;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderDetailRepository extends JpaRepository<OrderDetail, OrderDetailPK> {

  List<OrderDetail> findById_OrderNumber(Long orderNumber);

  void deleteById_OrderNumber(Long orderNumber);
}
