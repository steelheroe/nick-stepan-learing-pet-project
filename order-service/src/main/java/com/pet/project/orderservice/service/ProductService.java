package com.pet.project.orderservice.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.pet.project.orderservice.domain.dto.product.GetProductResponse;
import com.pet.project.orderservice.domain.dto.product.SaveProductRequest;
import com.pet.project.orderservice.domain.entity.Product;
import com.pet.project.orderservice.persistence.repository.ProductRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.ProductMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class ProductService {

  private final ObjectMapper objectMapper;
  private final ProductRepository productRepository;
  private final ProductMapper productMapper;

  public GetProductResponse findById(final long id) {
    return productMapper.productToGetProductResponse(
        productRepository.findById(id).orElseThrow(() ->
            new EntityDoesNotExistException("Product with id: " + id + " does not exist."))
    );
  }

  public List<GetProductResponse> findAll() {
    return productMapper.productToGetProductResponse(productRepository.findAll());
  }

  public GetProductResponse save(final SaveProductRequest newProduct) {
    return productMapper.productToGetProductResponse(productRepository.save(
        productMapper.saveProductToProductMapping(newProduct))
    );
  }

  public void delete(final long id) {
    productRepository.deleteById(id);
  }

  public GetProductResponse update(final long id, final SaveProductRequest newProduct) {
    return productMapper.productToGetProductResponse(
        productRepository.save(productMapper.saveProductToProductMapping(newProduct).setCode(id))
    );
  }

  public GetProductResponse patch(final Long productId, final JsonPatch patch)
      throws JsonProcessingException, JsonPatchException {
    return patch(findByIdModel(productId), patch);
  }

  private GetProductResponse patch(final Product product, final JsonPatch patch)
      throws JsonProcessingException, JsonPatchException {
    JsonNode patched = patch.apply(objectMapper.convertValue(product, JsonNode.class));

    return productMapper.productToGetProductResponse(
        productRepository.save(objectMapper.treeToValue(patched, Product.class))
    );
  }

  private Product findByIdModel(final long id) {
    return productRepository.findById(id).orElseThrow(() ->
        new EntityDoesNotExistException("Product with id: " + id + " does not exist.")
    );
  }
}
