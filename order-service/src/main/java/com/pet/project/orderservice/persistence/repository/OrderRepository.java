package com.pet.project.orderservice.persistence.repository;

import com.pet.project.orderservice.domain.entity.Order;
import com.pet.project.orderservice.domain.entity.projection.OrderWithDetailsProductIdsProjection;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {

  @EntityGraph(attributePaths = {"orderDetails"})
  List<OrderWithDetailsProductIdsProjection> findAllProjectedBy();

}
