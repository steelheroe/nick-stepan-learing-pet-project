package com.pet.project.orderservice.domain.dto.orderdetail;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Data
@Accessors
public class CreateUpdateOrderDetailRequest {

  @Min(0)
  private long quantityOrdered;

  @Min(0)
  private BigDecimal priceForEach;
}
