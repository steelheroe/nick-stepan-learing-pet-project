package com.pet.project.orderservice.util.exception;

/**
 * Class representing an exception that should be thrown in case of wrong input data for controllers.
 */
public class EntityDoesNotExistException extends RuntimeException {

  public EntityDoesNotExistException(final String message) {
    super(message);
  }
}
