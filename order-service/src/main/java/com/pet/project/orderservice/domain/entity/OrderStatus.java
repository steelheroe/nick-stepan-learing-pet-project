package com.pet.project.orderservice.domain.entity;

public enum OrderStatus {
  CREATED, DECLINED, CONFIRMED, SHIPPED;
}
