package com.pet.project.orderservice.domain.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Set;

@Data
@Accessors
@Entity
@Table(name = "order")
public class Order {

  @Id
  @Column(name = "order_number")
  @SequenceGenerator(name = "order_generator", sequenceName = "product_id_seq", allocationSize = 1, initialValue = 10)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_generator")
  private long orderNumber;

  @NotNull
  @Column(name = "order_date")
  private Timestamp orderDate;

  @NotNull
  @Column(name = "shipping_date")
  private Timestamp shippingDate;

  @Enumerated(EnumType.STRING)
  @Column(name = "status")
  private OrderStatus status;

  @Column(name = "comment")
  private String comment;

  @Column(name = "customer_number")
  private String customerNumber;

  @Column(name = "customer_name")
  private String customerName;

  @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
  @JoinColumn(name = "order_number", referencedColumnName = "order_number")
  private Set<OrderDetail> orderDetails;
}
