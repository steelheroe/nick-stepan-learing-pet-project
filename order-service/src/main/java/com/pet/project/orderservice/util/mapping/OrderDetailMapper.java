package com.pet.project.orderservice.util.mapping;

import com.pet.project.orderservice.domain.dto.orderdetail.CreateUpdateOrderDetailRequest;
import com.pet.project.orderservice.domain.dto.orderdetail.GetOrderDetailResponse;
import com.pet.project.orderservice.domain.entity.OrderDetail;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, componentModel = "spring")
public interface OrderDetailMapper {

  @Mapping(source = "id.orderNumber", target = "orderNumber")
  @Mapping(source = "id.productNumber", target = "productNumber")
  @Mapping(source = "quantityOrdered", target = "quantityOrdered")
  @Mapping(source = "priceForEach", target = "priceForEach")
  GetOrderDetailResponse orderDetailToGetOrderDetailResponse(OrderDetail source);

  List<GetOrderDetailResponse> orderDetailToGetOrderDetailResponse(List<OrderDetail> source);

  @Mapping(source = "quantityOrdered", target = "quantityOrdered")
  @Mapping(source = "priceForEach", target = "priceForEach")
  @Mapping(target = "id.orderNumber", ignore = true)
  @Mapping(target = "id.productNumber", ignore = true)
  OrderDetail saveOrderDetailRequestToOrderDetail(CreateUpdateOrderDetailRequest source);

  List<OrderDetail> saveOrderDetailRequestToOrderDetail(List<CreateUpdateOrderDetailRequest> source);

  default Long orderDetailToProductId(OrderDetail source) {
    return source.getId().getProductNumber();
  }

  List<Long> orderDetailToProductId(List<OrderDetail> source);
}
