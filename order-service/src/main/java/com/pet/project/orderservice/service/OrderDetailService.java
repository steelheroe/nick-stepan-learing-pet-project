package com.pet.project.orderservice.service;

import com.pet.project.orderservice.domain.dto.orderdetail.CreateUpdateOrderDetailRequest;
import com.pet.project.orderservice.domain.dto.orderdetail.GetOrderDetailResponse;
import com.pet.project.orderservice.domain.entity.OrderDetail;
import com.pet.project.orderservice.domain.entity.OrderDetailPK;
import com.pet.project.orderservice.persistence.repository.OrderDetailRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.OrderDetailMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@AllArgsConstructor
@Service
public class OrderDetailService {

  private final OrderDetailRepository orderDetailRepository;
  private final OrderDetailMapper orderDetailMapper;

  public GetOrderDetailResponse findById(final long orderNumber, final long productNumber) {
    return findById(createOrderDetailPk(orderNumber, productNumber));
  }

  public List<GetOrderDetailResponse> findAllByOrderNumber(final long orderNumber) {
    return orderDetailMapper.orderDetailToGetOrderDetailResponse(
        orderDetailRepository.findById_OrderNumber(orderNumber)
    );
  }

  public void delete(final long orderNumber, final long productNumber) {
    delete(createOrderDetailPk(orderNumber, productNumber));
  }

  public GetOrderDetailResponse update(final long orderId, final long productId,
                                       final CreateUpdateOrderDetailRequest updatedOrderDetail) {
    OrderDetail orderDetail = orderDetailMapper.saveOrderDetailRequestToOrderDetail(updatedOrderDetail);
    orderDetail.getId().setProductNumber(productId).setOrderNumber(orderId);

    return orderDetailMapper.orderDetailToGetOrderDetailResponse(
        orderDetailRepository.save(orderDetail)
    );
  }

  @Transactional
  public void deleteByOrderNumber(final long orderNumber) {
    orderDetailRepository.deleteById_OrderNumber(orderNumber);
  }

  private OrderDetailPK createOrderDetailPk(final Long orderNumber, final Long productNumber) {
    return new OrderDetailPK(orderNumber, productNumber);
  }

  private GetOrderDetailResponse findById(final OrderDetailPK id) {
    return orderDetailMapper.orderDetailToGetOrderDetailResponse(
        orderDetailRepository.findById(id).orElseThrow(() ->
            new EntityDoesNotExistException("OrderDetail with id: " + id + " does not exist."))
    );
  }

  private void delete(final OrderDetailPK id) {
    orderDetailRepository.deleteById(id);
  }
}
