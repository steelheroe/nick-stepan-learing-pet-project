package com.pet.project.orderservice.domain.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Accessors
@Entity
@Table(name = "order_detail")
public class OrderDetail {

  @Id
  private OrderDetailPK id;

  @Column(name = "quantity_ordered")
  private long quantityOrdered;

  @Column(name = "price_for_each")
  private BigDecimal priceForEach;
}
