package com.pet.project.orderservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import com.pet.project.orderservice.domain.dto.product.GetProductResponse;
import com.pet.project.orderservice.domain.dto.product.SaveProductRequest;
import com.pet.project.orderservice.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/products")
@Validated
public class ProductController {

  private final ProductService productService;

  @GetMapping("/{productId}")
  public GetProductResponse getProduct(@PathVariable @Min(1) final Long productId) {
    return productService.findById(productId);
  }

  @GetMapping
  public List<GetProductResponse> getProducts() {
    return productService.findAll();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public GetProductResponse createNewProduct(@Valid @RequestBody final SaveProductRequest newProduct) {
    return productService.save(newProduct);
  }

  @DeleteMapping("/{productId}")
  public void deleteProduct(@PathVariable @Min(1) final Long productId) {
    productService.delete(productId);
  }

  @PutMapping("/{productId}")
  public GetProductResponse updateProduct(@PathVariable @Min(1) final Long productId,
                                          @Valid @RequestBody final SaveProductRequest newProduct) {
    return productService.update(productId, newProduct);
  }

  @PatchMapping("/{productId}")
  public GetProductResponse patchProduct(@PathVariable @Min(1) final Long productId,
                                         final @RequestBody JsonPatch patch)
      throws JsonPatchException, JsonProcessingException {
    return productService.patch(productId, patch);
  }
}
