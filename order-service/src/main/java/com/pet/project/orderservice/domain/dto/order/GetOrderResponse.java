package com.pet.project.orderservice.domain.dto.order;

import lombok.Data;
import lombok.experimental.Accessors;

import java.sql.Timestamp;
import java.util.Set;

@Data
@Accessors
public class GetOrderResponse {

  private long orderNumber;

  private Timestamp orderDate;

  private Timestamp shippingDate;

  private String status;

  private String comment;

  private String customerNumber;

  private String customerName;

  private Set<Long> orderDetailProductIDs;
}
