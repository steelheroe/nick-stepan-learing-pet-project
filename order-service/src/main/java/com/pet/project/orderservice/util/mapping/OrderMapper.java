package com.pet.project.orderservice.util.mapping;

import com.pet.project.orderservice.domain.dto.order.GetOrderResponse;
import com.pet.project.orderservice.domain.dto.order.SaveOrderRequest;
import com.pet.project.orderservice.domain.entity.Order;
import com.pet.project.orderservice.domain.entity.projection.OrderWithDetailsProductIdsProjection;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    uses = OrderDetailMapper.class, componentModel = "spring")
public abstract class OrderMapper {

  @Mapping(source = "orderDate", target = "orderDate")
  @Mapping(source = "shippingDate", target = "shippingDate")
  @Mapping(source = "status", target = "status")
  @Mapping(source = "comment", target = "comment")
  @Mapping(source = "customerName", target = "customerName")
  @Mapping(source = "customerNumber", target = "customerNumber")
  @Mapping(target = "orderNumber", ignore = true)
  @Mapping(target = "orderDetails", ignore = true)
  public abstract Order saveOrderRequestToOrderEntityMapping(SaveOrderRequest source);

  public abstract List<Order> saveOrderRequestToOrderEntityMapping(List<SaveOrderRequest> source);

  @Mapping(source = "orderNumber", target = "orderNumber")
  @Mapping(source = "orderDate", target = "orderDate")
  @Mapping(source = "shippingDate", target = "shippingDate")
  @Mapping(source = "status", target = "status")
  @Mapping(source = "comment", target = "comment")
  @Mapping(source = "customerNumber", target = "customerNumber")
  @Mapping(source = "customerName", target = "customerName")
  @Mapping(source = "orderDetails", target = "orderDetailProductIDs")
  public abstract GetOrderResponse orderEntityToGetOrderResponse(Order source);

  public abstract List<GetOrderResponse> orderEntityToGetOrderResponse(List<Order> source);


  @Mapping(source = "orderNumber", target = "orderNumber")
  @Mapping(source = "orderDate", target = "orderDate")
  @Mapping(source = "shippingDate", target = "shippingDate")
  @Mapping(source = "status", target = "status")
  @Mapping(source = "comment", target = "comment")
  @Mapping(source = "customerNumber", target = "customerNumber")
  @Mapping(source = "customerName", target = "customerName")
  @Mapping(source = "orderDetails", target = "orderDetailProductIDs",
      qualifiedByName = "detailProductIdProjectionToLong")
  public abstract GetOrderResponse orderWithDetailsProductIdsProjectionToGetOrderResponse(
      OrderWithDetailsProductIdsProjection source);

  public abstract List<GetOrderResponse> orderWithDetailsProductIdsProjectionToGetOrderResponse(
      List<OrderWithDetailsProductIdsProjection> source);

  @Named("detailProductIdProjectionToLong")
  public Set<Long> detailProductIdProjectionToLong(
      final Set<OrderWithDetailsProductIdsProjection.OrderDetailWithOnlyIdProjection> source) {
    return source.stream().map(a -> a.getId().getProductNumber()).collect(Collectors.toSet());
  }
}
