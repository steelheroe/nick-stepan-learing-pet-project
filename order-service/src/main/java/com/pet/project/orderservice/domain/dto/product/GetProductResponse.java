package com.pet.project.orderservice.domain.dto.product;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Getter
@Accessors
public class GetProductResponse {

  private Long code;

  private String name;

  private BigDecimal price;

  private String description;

  private long quantityInStock;
}
