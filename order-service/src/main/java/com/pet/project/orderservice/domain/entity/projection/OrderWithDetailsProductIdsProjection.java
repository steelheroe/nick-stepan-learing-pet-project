package com.pet.project.orderservice.domain.entity.projection;

import com.pet.project.orderservice.domain.entity.OrderStatus;

import java.sql.Timestamp;
import java.util.Set;

public interface OrderWithDetailsProductIdsProjection {

  long getOrderNumber();

  Timestamp getOrderDate();

  Timestamp getShippingDate();

  OrderStatus getStatus();

  String getComment();

  String getCustomerNumber();

  String getCustomerName();

  Set<OrderDetailWithOnlyIdProjection> getOrderDetails();

  interface OrderDetailWithOnlyIdProjection {
    OrderDetailPKProductIdProjection getId();

    interface OrderDetailPKProductIdProjection {

      long getProductNumber();
    }
  }
}
