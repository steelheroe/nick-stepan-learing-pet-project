package com.pet.project.orderservice.domain.entity;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
@Accessors
@Entity
@Table(name = "product")
public class Product {

  @Id
  @Column(name = "code")
  @SequenceGenerator(name = "product_generator", sequenceName = "product_id_seq", allocationSize = 1, initialValue = 10)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_generator")
  private Long code;

  @NotEmpty
  @Column(name = "name", nullable = false)
  private String name;

  @Column(name = "price", nullable = false)
  @Min(value = 0, message = "Price should be equal or greater then 0.")
  private BigDecimal price;

  @Column(name = "description")
  private String description;

  @Min(value = 0, message = "Quantity should be equal or greater then 0.")
  @Column(name = "quantity_in_stock")
  private long quantityInStock;
}
