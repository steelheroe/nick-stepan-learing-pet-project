package com.pet.project.orderservice.domain.dto.orderdetail;

import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors
public class GetOrderDetailResponse {

  private long orderNumber;

  private long productNumber;

  private long quantityOrdered;

  private BigDecimal priceForEach;
}
