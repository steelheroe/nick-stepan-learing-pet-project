package com.pet.project.orderservice.domain.dto.product;

import lombok.Data;
import lombok.Getter;
import lombok.experimental.Accessors;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@Data
@Getter
@Accessors
public class SaveProductRequest {

  @NotEmpty
  private String name;

  @Min(value = 0, message = "Price should be equal or greater then 0.")
  private BigDecimal price;

  private String description;

  @Min(value = 0, message = "Quantity should be equal or greater then 0.")
  private long quantityInStock;
}
