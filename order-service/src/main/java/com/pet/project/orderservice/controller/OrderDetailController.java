package com.pet.project.orderservice.controller;

import com.pet.project.orderservice.domain.dto.orderdetail.CreateUpdateOrderDetailRequest;
import com.pet.project.orderservice.domain.dto.orderdetail.GetOrderDetailResponse;
import com.pet.project.orderservice.service.OrderDetailService;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/orders/{orderNumber}/orderDetails")
@Validated
public class OrderDetailController {

  private final OrderDetailService orderDetailService;

  @GetMapping("/{productNumber}")
  public GetOrderDetailResponse getOrder(@PathVariable @Min(1) final Long orderNumber,
                                         @PathVariable @Min(1) final Long productNumber) {
    return orderDetailService.findById(orderNumber, productNumber);
  }

  @GetMapping
  public List<GetOrderDetailResponse> getOrders(@PathVariable @Min(1) final Long orderNumber) {
    return orderDetailService.findAllByOrderNumber(orderNumber);
  }

  @DeleteMapping("/{productNumber}")
  public void deleteOrder(@PathVariable @Min(1) final Long orderNumber,
                                    @PathVariable @Min(1) final Long productNumber) {
    orderDetailService.delete(orderNumber, productNumber);
  }

  @DeleteMapping()
  public void deleteOrder(@PathVariable @Min(1) final Long orderNumber) {
    orderDetailService.deleteByOrderNumber(orderNumber);
  }

  @PutMapping("/{productNumber}")
  public GetOrderDetailResponse updateOrder(@PathVariable @Min(1) final Long orderNumber,
                                    @PathVariable @Min(1) final Long productNumber,
                                    @Valid @RequestBody final CreateUpdateOrderDetailRequest updatedOrderDetail) {
    return orderDetailService.update(orderNumber, productNumber, updatedOrderDetail);
  }
}
