package com.pet.project.orderservice.domain.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Embeddable
public class OrderDetailPK implements Serializable {

  @Column(name = "order_number")
  private long orderNumber;

  @Column(name = "product_number")
  private long productNumber;
}
