package com.pet.project.orderservice.persistence.repository;

import com.pet.project.orderservice.domain.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
