package com.pet.project.orderservice.util.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@AllArgsConstructor
@Data
public class ErrorResponse {
  private String message;

  private Map<String, String> errors;

  public ErrorResponse(final String message) {
    this.message = message;
  }

  public ErrorResponse(final Map<String, String> errors) {
    this.errors = errors;
  }
}
