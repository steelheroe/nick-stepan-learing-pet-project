package com.pet.project.orderservice.service;

import com.pet.project.orderservice.domain.dto.order.GetOrderResponse;
import com.pet.project.orderservice.domain.dto.order.SaveOrderRequest;
import com.pet.project.orderservice.persistence.repository.OrderRepository;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.mapping.OrderMapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class OrderService {

  private final OrderRepository orderRepository;
  private final OrderMapper orderMapper;

  public GetOrderResponse save(final SaveOrderRequest newOrder) {
    return orderMapper.orderEntityToGetOrderResponse(
        orderRepository.save(orderMapper.saveOrderRequestToOrderEntityMapping(newOrder))
    );
  }

  public GetOrderResponse findById(final long id) {
    return orderMapper.orderEntityToGetOrderResponse(
        orderRepository.findById(id).orElseThrow(() ->
            new EntityDoesNotExistException("Order with id: " + id + " does not exist."))
    );
  }

  public List<GetOrderResponse> findAll() {
    return orderMapper.orderWithDetailsProductIdsProjectionToGetOrderResponse(orderRepository.findAllProjectedBy());
  }

  public void delete(final long id) {
    orderRepository.deleteById(id);
  }

  public GetOrderResponse update(final long id, final SaveOrderRequest newOrder) {
    return orderMapper.orderEntityToGetOrderResponse(
        orderRepository.save(orderMapper.saveOrderRequestToOrderEntityMapping(newOrder).setOrderNumber(id))
    );
  }
}
