package com.pet.project.orderservice.controller;

import com.pet.project.orderservice.domain.dto.order.GetOrderResponse;
import com.pet.project.orderservice.domain.dto.order.SaveOrderRequest;
import com.pet.project.orderservice.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/orders")
@Validated
public class OrderController {

  private final OrderService orderService;

  @GetMapping("/{orderId}")
  public GetOrderResponse getOrder(@PathVariable @Min(1) final Long orderId) {
    return orderService.findById(orderId);
  }

  @GetMapping
  public List<GetOrderResponse> getOrders() {
    return orderService.findAll();
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public GetOrderResponse createNewOrder(@Valid @RequestBody final SaveOrderRequest newOrder) {
    return orderService.save(newOrder);
  }

  @DeleteMapping("/{orderId}")
  public void deleteOrder(@PathVariable @Min(1) final Long orderId) {
    orderService.delete(orderId);
  }

  @PutMapping("/{orderId}")
  public GetOrderResponse updateOrder(@PathVariable @Min(1) final Long orderId,
                                      @Valid @RequestBody final SaveOrderRequest newOrder) {
    return orderService.update(orderId, newOrder);
  }
}
