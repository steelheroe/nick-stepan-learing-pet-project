package com.pet.project.orderservice.util.exception.handler;

import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import com.github.fge.jsonpatch.JsonPatchException;
import com.pet.project.orderservice.util.exception.EntityDoesNotExistException;
import com.pet.project.orderservice.util.exception.ErrorResponse;
import com.pet.project.orderservice.util.exception.InvalidInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Class with overridden hibernate validation handlers. Also has handlers for the custom exceptions.
 */
@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ErrorResponse handleMethodArgumentNotValid(final MethodArgumentNotValidException ex) {
    Map<String, String> errors = new HashMap<>();

    ex.getBindingResult().getAllErrors().forEach((error) -> {
      String fieldName = ((FieldError) error).getField();
      String errorMessage = error.getDefaultMessage();
      errors.put(fieldName, errorMessage);
    });

    return new ErrorResponse(errors);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ErrorResponse handleHttpMessageNotReadable(final HttpMessageNotReadableException ex) {
    String resultMessage = ex.getMessage();
    Throwable rootCause = ex.getRootCause();
    if (Objects.nonNull(rootCause)) {
      String rootMessage = rootCause.getMessage();
      resultMessage = rootMessage;

      if (rootCause instanceof InvalidTypeIdException && rootMessage.contains("JsonPatchOperation")) {
        resultMessage = "Unknown operation";
      }
      if (rootCause instanceof UnrecognizedPropertyException) {
        resultMessage = rootMessage.substring(0, rootMessage.indexOf('('));
      }
    }

    return new ErrorResponse(resultMessage);
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(InvalidInputException.class)
  public ErrorResponse handleInvalidInput(final InvalidInputException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ResponseStatus(HttpStatus.NOT_FOUND)
  @ExceptionHandler(EntityDoesNotExistException.class)
  public ErrorResponse handleAbsentEntity(final EntityDoesNotExistException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(JsonPatchException.class)
  public ErrorResponse handleJsonPatchException(final JsonPatchException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(ConstraintViolationException.class)
  public ErrorResponse handleInvalidControllersInput(final ConstraintViolationException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(IllegalArgumentException.class)
  public ErrorResponse handleIllegalArgument(final IllegalArgumentException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ResponseStatus(HttpStatus.BAD_REQUEST)
  @ExceptionHandler(EmptyResultDataAccessException.class)
  public ErrorResponse emptyResultDataAccessException(final EmptyResultDataAccessException ex) {
    return new ErrorResponse(ex.getMessage());
  }
}
