Feature: Testing the rest api methods for the product entity

  Scenario: retrieving the list of the all products
    Given User decides to get a list of the products
    When User requests the list of the products
    Then User receives the list

  Scenario: retrieving a single product entity
    Given Product API '/products/'
    And retrieving existing product entity
    When Call given API with 'get' request
    Then Status_code equals 200
    And response contains a single entity

  Scenario: retrieving an absent product entity
    Given Product API '/products/-1'
    When Call given API with 'get' request
    Then Status_code equals 400

  Scenario: saving a new product entity
    Given Product API '/products'
    When Call given API with 'post' request
    Then Status_code equals 201
    And response contains new product entity

  Scenario: deleting an existing product entity
    Given Product API '/products/'
    And retrieving existing product entity
    When Call given API with 'delete' request
    Then Status_code equals 200
