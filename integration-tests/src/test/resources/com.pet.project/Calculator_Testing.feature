Feature: Calculator testing

  Scenario Outline: trying the add functionality
    Given Two numbers - <numberOne> and <numberTwo>
    When They are being added
    Then Verify that the result is <resultNumber>

    Examples:
    | numberOne | numberTwo | resultNumber |
    | 1         | 1         | 2            |
    | 3         | 2         | 5            |
