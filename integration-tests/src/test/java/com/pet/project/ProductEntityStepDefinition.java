package com.pet.project;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductEntityStepDefinition {

	private static final String BASE_URL = "http://localhost:8080";
	RequestSpecification request;
	private Response response;
	private String retrievedCode;

	@Given("Product API {string}")
	public void getProductListAPI(String link) {
		RestAssured.baseURI = BASE_URL + link;
		request = RestAssured.given();
		request.header("Content-Type", "application/json");
	}

	@When("Call given API with {string} request")
	public void callGivenAPI(String requestType) {
		if (requestType.equalsIgnoreCase("get")) {
			response = request.get(retrievedCode != null ? retrievedCode : "");
		}
		if (requestType.equalsIgnoreCase("post")) {
			response = request.body("{\n" +
					"        \"name\": \"Keychron k1 v4 TKL\",\n" +
					"        \"price\": 120.00,\n" +
					"        \"description\": \"Best tkl mechanical keyboard ever!!!!\",\n" +
					"        \"quantityInStock\": 150\n" +
					"    }").post();
		}
		if (requestType.equalsIgnoreCase("delete")){
			response = request.delete(retrievedCode);
		}
	}

	@Then("Status_code equals {int}")
	public void status_codeEquals(int arg0) {
		assertThat(response.getStatusCode()).isEqualTo(arg0);
	}

	@And("response contains new product entity")
	public void responseContainsNewProductEntity() {
		assertThat(getJsonPath(response, "code")).isNotBlank();
	}

	@And("retrieving existing product entity")
	public void retrievingExistingEntityFromThatList() {
		RequestSpecification retrievingRequest = RestAssured.given();
		retrievingRequest.header("Content-Type", "application/json");
		Response retrievingResponse = retrievingRequest.get();

		String codesList = getJsonPath(retrievingResponse, "code");
		String[] codes = codesList.substring(1, codesList.length() - 1).split(",");
		retrievedCode = codes[codes.length-1].trim();
	}

	@And("response contains a single entity")
	public void responseContainsASingleEntity() {
		assertThat(getJsonPath(response, "code")).isEqualTo(retrievedCode);
	}

	public String getJsonPath(Response response, String key) {
		String resp = response.asString();
		JsonPath js = new JsonPath(resp);
		return js.get(key).toString();
	}

	@Given("User decides to get a list of the products")
	public void userDecidesToGetAListOfTheProducts() {
		RestAssured.baseURI = BASE_URL + "/products/";
		request = RestAssured.given();
		request.header("Content-Type", "application/json");
	}

	@When("User requests the list of the products")
	public void userRequestsTheListOfTheProducts() {
		response = request.get();
	}

	@Then("User receives the list")
	public void userReceivesTheList() {
		assertThat(response.getStatusCode()).isEqualTo(200);
		assertThat(getJsonPath(response, "name")).isNotBlank();
	}
}
