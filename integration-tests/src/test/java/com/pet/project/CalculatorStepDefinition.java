package com.pet.project;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;

public class CalculatorStepDefinition {

	private int numberOne;
	private int numberTwo;

	private int result;

	@Given("Two numbers - {int} and {int}")
	public void given_two_numbers(int numberOne, int numberTwo){
		this.numberOne = numberOne;
		this.numberTwo = numberTwo;

		System.out.println(numberOne);
		System.out.println(numberTwo);
	}

	@When("They are being added")
	public void being_added(){
		result = numberOne + numberTwo;
	}

	@Then("Verify that the result is {int}")
	public void verify_the_result(int resultNumber){
		assertThat(result).isEqualTo(resultNumber);
	}

}
